package com.example.springkafka.controller;

import com.example.springkafka.services.KafkaProducer;
import com.example.springkafka.storage.MessageStorage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


import java.util.Map;

@RestController
@RequestMapping(value="/spring-kafka")
public class WebRestController {
	
	@Autowired
    KafkaProducer producer;
	
	@Autowired
    MessageStorage storage;
	
	@GetMapping(value="/producer")
	public String producer(@RequestParam("data")String data){
		producer.send(data);
		
		return "Done";
	}
	
	@GetMapping(value="/consumer")
	public String getAllRecievedMessage(){
		String messages = storage.toString();
		storage.clear();
		
		return messages;
	}


	@RequestMapping(
			value = "/hub-comunicaciones",
			method = RequestMethod.POST)
	public void process(@RequestBody Map<String, Object> payload)
			throws Exception {

		System.out.println(payload);
		producer.send(payload.toString());

	}

}
